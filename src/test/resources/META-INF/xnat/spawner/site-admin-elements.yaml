siteId:
    label: Site ID
    type: text
    id: site-id
    description: Identifies your XNAT site.
    placeholder: Enter your XNAT site ID...
    url: /xapi/services/prefs/siteId/{siteId}
    default: XNAT
    validation:
        required: true
        type: xnat-id

siteUrl:
    label: Site Url
    type: url
    description: The root URL for the site. This is passed to external services.
    placeholder: Enter the URL for your XNAT site...
    value: https://cnda.wustl.edu
    validation:
        required: true

siteDescriptionPage:
    id: desc-page
    label: Page
    type: site.path
    description: The page to display for the site description, e.g. /screens/site_description.vm.
    value: /screens/site_description.vm
    placeholder: Enter a page for your site description...
    validation:
        required: true

siteDescriptionMarkdown:
    id: desc-markdown
    label: Text (Markdown)
    type: markdown
    tooltip: XNAT allows you to use GitHub-flavored Markdown to create and format your own site description. [&gt;&gt; View Tutorial](http://foobar)
    description: Compose a site description without referencing a template or site page.
    validation:
        required: true

siteDescription:
    label: Site Description
    type: composite
    selection: radio
    url: /data/services/prefs/{desc}
    children:
        ${siteDescriptionPage}
        ${siteDescriptionMarkdown}

landingPage:
    label: Landing Page
    type: site.path
    description: The page to display when the user logs in.
    value: /screens/QuickSearch.vm
    placeholder: Enter the default landing page...
    overrides:
        target: homePage
        type: checkbox
        position: right
        description: Use this as my home page.
        hideTarget: false
    validation:
        required: true

homePage:
    label: Home Page
    type: site.path
    description: The page to display when the user clicks the home link.
    value: /screens/AdminUsers.vm
    placeholder: Enter the default home page...
    validation:
        required: true

siteInfo:
    label: Site Information
    type: panel
    controls:
       ${siteId}
       ${siteUrl}
       ${siteDescription}
       ${landingPage}
       ${homePage}

siteAdminEmail:
    label: Site Admin Email
    type: email
    placeholder: Administrator email address...
    url: /xapi/services/prefs/siteAdminEmail/{siteAdminEmail}
    validation:
        required: true

adminInfo:
    label: Admin Information
    type: panel
    controls:
        ${siteAdminEmail}

siteSetup:
    label: Site Setup
    type: tab
    contents:
        ${siteInfo}
        ${adminInfo}

xnatSetup:
    label: XNAT Setup
    type: tabGroup
    tabs:
        ${siteSetup}

siteAdmin:
    label: Administer XNAT
    description: Administrative functions to configuring and managing XNAT.
    type: page
    contains: tabs
    contents:
        ${xnatSetup}
